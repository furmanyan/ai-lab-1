import random
import threading

from bot import Bot

import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType

from config import vk_api_token

def write_msg(user_id, message):
    vk.method('messages.send', {'user_id': user_id, 'message': message, 'random_id': random.randint(0, 2048)})

def messageThread(messageEvent):
    bot = Bot(messageEvent.user_id)

    write_msg(messageEvent.user_id, bot.new_message(messageEvent.text))

    print('Text: ', messageEvent.text)
    print("-------------------")

# API-ключ созданный ранее
token = vk_api_token

# Авторизуемся как сообщество
vk = vk_api.VkApi(token=token)

# Работа с сообщениями
longpoll = VkLongPoll(vk)

print("Server started")
for event in longpoll.listen():
    if event.type == VkEventType.MESSAGE_NEW:
        if event.to_me:
            thread=threading.Thread(target=messageThread,args=(event,))
            thread.start()
            thread.join()