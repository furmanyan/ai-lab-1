import requests
import asyncio
import decimal
import re
from datetime import datetime

import mysql.connector
from config import db_name
from config import db_password

# Allows dates following these rules:
# - days may have leading zeros. 1-31. max 2 digits
# - months may have leading zeros. 1-12. max 2 digits
# - years 1900-2099. 4 digits
date_regex = "(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})"

# Matches a string with a 24-hour time format of HH:MM
time_regex = "([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])"

# Matches decimal with up to 65 digits and 2 digits after comma
sum_regex = "\d{1,65}(?:[\.,]{1}\d{1,2}){0,1}"

name_regex = "(?:[\wа-яА-Я]+[\.,\s]*)+"

# (?i)^\s*\/(spend|earn)\s+((?:[\wа-яА-Я]+[\.,\s]*)+)\s+\/sum\s+(\d{1,65}(?:[\.,]{1}\d{1,2}){0,1})\s+\/date\s+(?:((3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2}))|(?:сегодня\s+(([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]))))\s*$
# группа 1 - доход или расход
# группа 2 - имя
# группа 3 - сумма
# группа 4 - дата (null если было введено "сегодня")
# группа 5 - день (null если было введено "сегодня")
# группа 6 - месяц (null если было введено "сегодня")
# группа 7 - год (null если было введено "сегодня")
# группа 8 - время (null если была введена дата)
# группа 9 - час (null если была введена дата)
# группа 10 - минута (null если была введена дата)
add_msg_pattern = "(?i)^\s*\/(spend|earn)\s+(" + name_regex + ")+\s+\/sum\s+(" + sum_regex + ")\s+\/date\s+(?:(" + date_regex + ")|(?:сегодня\s+(" + time_regex+")))\s*$"
show_all_pattern = "(?i)^\s*показать\s+список\s*$"
delete_all_pattern = "(?i)^\s*очистить\s+список\s*$"
hello_pattern = "(?i)^[\/\\\\]?\s*(привет|начать|start)\s*$"

class Bot:
    def __init__(self, user_id):
        self.USER_ID = user_id
        # если такого пользователя нет
        if not self._checkUserInDB() :
            # добавить пользователя в БД
            self._addUserInDB()
        print("Bot initialised")

    def _addUserInDB(self):
        try:
            print('connecting to MySQL database...')
            conn = mysql.connector.connect(host='localhost', database=db_name, user='user', password=db_password)
            if conn.is_connected():
                print('connection: succeed')
            else:
                print('connection: failed')
            args = (self.USER_ID,)
            addQuery = 'insert into user (id) value (%s)'
            add = conn.cursor()
            add.execute(addQuery, args)
            conn.commit()
            print("Новый пользователь добавлен")
        except mysql.connector.Error as error:
            print(error)
            print("ОШИБКА: новый пользователь не добавлен")
        finally:
            try:
                add.close()
                conn.close()
            except:
                pass

    def _checkUserInDB(self):
        isCurUserAdded = False
        try:
            print('connecting to MySQL database...')
            conn = mysql.connector.connect(host='localhost', database=db_name, user='user', password=db_password)
            if conn.is_connected():
                print('connection: succeed')
            else:
                print('connection: failed')
            args = (self.USER_ID,)
            getQuery = 'select COUNT(*) from user where id = %s'
            get = conn.cursor()
            get.execute(getQuery, args)
            result = get.fetchall()
            curUserCount = result[0][0]
            isCurUserAdded = curUserCount > 0
        except mysql.connector.Error as error:
            print(error)
            print("ОШИБКА: наличие пользователя не проверено")
        finally:
            try:
                get.close()
                conn.close()
            except:
                pass
        return isCurUserAdded

    def _parse_InOutcome(self, match_result: re.Match):
        isSpend = str(match_result.group(1)) == "spend"
        name = str(match_result.group(2))
        sum = float(match_result.group(3))
        if isSpend :
            sum *= -1
        date_time = None
        if match_result.group(4) != None:
            day = int(match_result.group(5))
            month = int(match_result.group(6))
            year = int(match_result.group(7))
            date_time = datetime(year, month, day)
        elif match_result.group(8) != None:
            date_time = datetime.now()
            new_hour = int(match_result.group(9))
            new_minute = int(match_result.group(10))
            date_time = date_time.replace(hour = new_hour, minute = new_minute)
        return [name, sum, date_time]

    def _addInOutcome(self, name: str, sum: float, date_time: datetime):
        try:
            print('connecting to MySQL database...')
            conn = mysql.connector.connect(host='localhost', database=db_name, user='user', password=db_password)
            if conn.is_connected():
                print('connection: succeed')
            else:
                print('connection: failed')
            args = (self.USER_ID, name, str(date_time), sum)
            addQuery = 'insert into inoutcome (user_id, name, date_time, sum) value (%s, %s, %s, %s)'
            add = conn.cursor()
            add.execute(addQuery, args)
            conn.commit()
            print("Запись добавлена")
            return f"Запись добавлена"
        except mysql.connector.Error as error:
            print(error)
            print("ОШИБКА: запись не добавлена")
            return f"ОШИБКА: запись не добавлена"
        finally:
            try:
                add.close()
                conn.close()
            except:
                pass

    def _getAllInOutcomes(self):
        results = []
        try:
            print('connecting to MySQL database...')
            conn = mysql.connector.connect(host='localhost', database=db_name, user='user', password=db_password)
            if conn.is_connected():
                print('connection: succeed')
            else:
                print('connection: failed')
            args = (self.USER_ID,)
            getQuery = 'select name, sum, date_time from inoutcome where user_id = %s order by date_time'
            get = conn.cursor()
            get.execute(getQuery, args)
            results = get.fetchall()
        except mysql.connector.Error as error:
            print(error)
            print("ОШИБКА: запись не добавлена")
        finally:
            try:
                get.close()
                conn.close()
            except:
                pass
        return results

    def _deleteInOucomes(self):
        try:
            print('connecting to MySQL database...')
            conn = mysql.connector.connect(host='localhost', database=db_name, user='user', password=db_password)
            if conn.is_connected():
                print('connection: succeed')
            else:
                print('connection: failed')
            args = (self.USER_ID,)
            delQuery = 'delete from inoutcome where user_id = %s'
            delete = conn.cursor()
            delete.execute(delQuery, args)
            conn.commit()
            return f"Данные удалены"
        except mysql.connector.Error as error:
            print(error)
            return f"ОШИБКА: данные не удалены"
        finally:
            try:
                delete.close()
                conn.close()
            except:
                pass

    def new_message(self, in_message):
        match_InOutcome = re.match(add_msg_pattern, in_message)
        match_ShowAll = re.match(show_all_pattern, in_message)
        match_DeleteAll = re.match(delete_all_pattern, in_message)
        match_Hello = re.match(hello_pattern, in_message)
        # если введена команда добавления новой записи:
        if match_InOutcome:
            # распарсить строку по capturing group регулярного выражения
            data = self._parse_InOutcome(match_InOutcome)
            name = data[0]
            sum = data[1]
            date_time = data[2]
            # если дата больше сегодняшней, является 30 или 31 февраля, или 29 не в високоный год - вывести ошибку
            cur_date = datetime.today().date()
            if date_time.date() > cur_date :
                return f"ОШИБКА: Вы ввели дату больше текущей"
            if date_time.month == 2:
                if date_time.day > 29:
                    return f"ОШИБКА: Вы ввели {0} февраля".format(date_time.day)
                if date_time.year % 4 != 0 and date_time.day == 29:
                    return f"ОШИБКА: Вы ввели 29 февраля не в високосный год"
            # добавить запись в БД
            return self._addInOutcome(name, sum, date_time)
        # иначе если введена команда вывода всех записей
        elif match_ShowAll:
            # получить все записи
            InOutcomes = self._getAllInOutcomes()
            if InOutcomes == []:
                return f"Записи отсутствуют"
            # рассчитать баланс
            balance = 0.0
            # вывести записи и баланс
            returnMsg = "--------------------\nВаши доходы и расходы:\n--------------------\n"
            for InOutcome in InOutcomes:
                name = str(InOutcome[0])
                sum = float(InOutcome[1])
                balance += sum
                date_time = InOutcome[2]
                isOutcome = sum < 0
                if isOutcome:
                    returnMsg += "Расход\n"
                    sum *= -1
                else:
                    returnMsg += "Доход\n"
                returnMsg += "Название: "+name+"\n"
                returnMsg += "Сумма: "+"{:.2f}".format(sum)+"\n"
                returnMsg += "Дата и время: "+"{:%d.%m.%Y  %H:%M}".format(date_time)+"\n--------------------\n"
            returnMsg += "БАЛАНС: "+"{:.2f}".format(balance)+"\n--------------------\n"
            return returnMsg
        # иначе если введена команда
        elif match_DeleteAll:
            # удалить записи из БД
            return self._deleteInOucomes()
        # Иначе если введена приветственная команда
        elif match_Hello:
            # Вывести приветствие
            return f"Добро пожаловать в бот для отслеживания трат и доходов!"
        # иначе
        else:
            # вывести ошибку ввода данных
            return f"Ошибка ввода данных"



